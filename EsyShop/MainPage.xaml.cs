﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace EsyShop
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async Task SendSms(string messageText, string recipient)
        {
            try
            {
                var message = new SmsMessage(messageText, recipient);
                await Sms.ComposeAsync(message);
            }
            catch (FeatureNotSupportedException ex)
            {
                await DisplayAlert("Failed", "Sms is not supported on this device.", "OK");
            }
            catch (Exception ex)
            {
                await DisplayAlert("Failed", ex.Message, "OK");
            }
        }
        async Task SendEmail(string subject, string body, List<string> recipients)
        {
            try
            {
                var message = new EmailMessage
                {
                    Subject = subject,
                    Body = body,
                    To = recipients,
                    //Cc = ccRecipients,
                    //Bcc = bccRecipients
                };
                await Email.ComposeAsync(message);
            }
            catch (FeatureNotSupportedException ex)
            {
                await DisplayAlert("Failed", "Email is not supported on this device.", "OK");
            }
            catch (Exception ex)
            {
                await DisplayAlert("Failed", ex.Message, "OK");
            }
        }

        void Webview_Navigated(System.Object sender, Xamarin.Forms.WebNavigatedEventArgs e)
        {
            if (e.Url != null)
            {
                if (e.Url.ToLower().StartsWith("tel:") ||
                    e.Url.ToLower().StartsWith("sms:") ||
                    e.Url.ToLower().StartsWith("mailto:"))
                {
                    webview.GoBack();
                }
            }
        }

        async void WebView_Navigating(System.Object sender, Xamarin.Forms.WebNavigatingEventArgs e)
        {
            if (e.Url.ToLower().StartsWith("tel:"))
            {
                e.Cancel = true;
                var phonenumber = e.Url.Split(':')[1].Trim();
                PhoneDialer.Open(phonenumber);
            }
            if (e.Url.ToLower().StartsWith("sms:"))
            {
                e.Cancel = true;
                var phonenumber = e.Url.Split(':')[1].Trim();
                await SendSms("nhập nội dung", phonenumber);
            }
            if (e.Url.ToLower().StartsWith("mailto:"))
            {
                e.Cancel = true;
                var phonenumber = e.Url.Split(':')[1].Trim();
                var recipients = new List<string>();
                recipients.Add(phonenumber);
                await SendEmail("Tiêu đề", "Nội dung", recipients);
            }

        }
    }
}
